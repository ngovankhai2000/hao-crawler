var express = require('express');
var router = express.Router();
var crawlerService = require("../crawlerService")
/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});
router.post("/", async (req, res) => {
  let { EMAIL, PASSWORD } = req.body;
  let data = await crawlerService(EMAIL, PASSWORD);
  return res.send(data);
});
module.exports = router;
